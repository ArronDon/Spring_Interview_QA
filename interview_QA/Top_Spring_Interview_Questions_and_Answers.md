# 1. IOC是什么或者什么是控制反转  
这个Spring面试题是走向Spring的第一步，很多面试也以这个问题开始。就像名字说的那样，控制反转意味着现在我们已经将创建对象的控制权从我们自己使用new关键字创建交给了Spring框架的容器。现在按照需求创建一个对象已经是Spring容器的责任了。
我们维护一个XML文件。在这个文件中，我们设置组件，服务，所有的类以及它们的属性。我们只需要提及哪个组件需要什么服务，然后Spring容器就会给我们创建对象。这个概念以“依赖注入”更为大家所熟知，因为所有的对象依赖（资源）是被Spring框架注入其中的。
```
<bean id="createNewStock" class="springexample.stockMarket.CreateNewStockAccont">         
      <property name="newBid"/>
</bean>
```   
在这个例子中，CreateNewStockAccount类包含了newBid属性的getter和setter。容器会自动实例化newBid并且自动设置它使用的值。这整个过程在Spring中也被称为绑定，通过使用注解可以被Spring自动完成，这就是Spring中Bean的自动绑定。
# 2. 解释Spring中Bean的生命周期
由于Spring是基于IOC，所以我们称它为IOC容器，Spring Beans存在于IOC容器内。SpringBean是就是普通的java对象。（POJO plain old java object）
接下来解释Bean在容器内的生命周期：
- 容器先查看配置文件（比如bean.xml）中bean的定义
- 容器利用反射创建对象，如果bean内部定义了任何属性，容器也会对它们进行设置
- 如果bean实现了`BeanNameAware`接口，工厂将会调用`setBeanName()`方法，传递bean的ID
- 如果bean实现了`BeanFactoryAware`接口，工厂将会调用 `setBeanFactory()`方法，传递一个自身实例
- 如果有任何和这个bean相关的`BeanPostProcessors`，它们 `ProcessBeforeInitialization()`将会在bean 的属性被设置之前调用。
- 如果bean中指定了`init()`方法，将会被调用
- 如果bean实现了`DisposableBean`接口，那么当应用不再需要这个bean引用的时候，`destroy()`方法将会被调用
- 如果配置文件中bean定义中包含了`destroy-method`属性，在这个类中相应的方法定义将会被调用

# 3. 什么是BeanFactory?你是否用过XMLBeanFactory 
# 4. Spring中BeanFactory和ApplicationContext区别是什么  
# 5. Spring中不同的模块有什么  
# 6. singleton和prototype区别  
# 7. Spring支持什么类型的transaction Management  
# 8. 什么是AOP  
AOP的核心结构是切面，切面就是将影响很多类的行为封装到可重用的模块中。AOP是一种允许开发者将横切面模块化的编程技巧，实现不同责任的典型分割，例如日志和事务管理。Spring AOP,切面可以被普通的类或者普通的使用了注解`@Aspect`的类实现。你也可以查看[Spring MVC面试问题](http://www.java67.com/2012/08/spring-interview-questions-answers.html)来更多的关注利用Spring框架进行Java Web开发
# 9. 解释Advice  
Advice是切面的一个实现：advice在切点处被插入到应用之中。advice有不同类型，包括：`around`,`before`,`after`
# 10. 什么是jointPoint和point cut  
# 11. Spring中setter和Constructor injection的不同  
# 12. 如何在Spring Web应用中简历JDBC数据库连接池  
# 13. 工厂模式和依赖注入的区别